#version 400
in vec2 tex_coord;
out vec4 frag_colour;

uniform sampler2D tex;
uniform vec3 HmdWarpParam;

void main()
{
    // Warp Params
    vec2  LensCenter = vec2(0.5, 0.5);
    vec2  ScreenCenter = vec2(0.5, 0.5);
    vec2  Scale = vec2(1, 1);
    vec2  ScaleIn = vec2(0.50, 0.50);

    vec2  uv      = tex_coord.xy;
    vec2  theta   = (uv - LensCenter) * ScaleIn;
    float rSq     = theta.x * theta.x + theta.y * theta.y;
    vec2  rvector = theta * (HmdWarpParam.x + HmdWarpParam.y * rSq +
                             HmdWarpParam.z * rSq * rSq + 0.0 * rSq * rSq * rSq);
    vec2  tc      = (LensCenter + Scale * rvector);
    
    //*** Adjust 0.5,0.5 according to how you render
    if (any(bvec2(clamp(tc, ScreenCenter-vec2(0.50,0.50), ScreenCenter+vec2(0.50,0.50))-tc))) 
        frag_colour = vec4(0.0, 0.0, 0.0, 1.0);
    else
        frag_colour = texture2D(tex, tc);
}