#version 400
uniform mat4 transform;
uniform vec3 transform2;
in vec2 vp;
in vec2 vt;
out vec2 tex_coord;

void main()
{
  gl_Position = vec4(vp.x + transform2.x, vp.y  + transform2.y, 0.0 + (-transform2.z), 1.0)  * transform;
  tex_coord = vt;
}