﻿using Bonsai;
using OpenCV.Net;
using System;
using System.ComponentModel;
using System.Reactive.Linq;

namespace Bonsai.Artica.Drawing
{
    [Description("Draws a text string.")]
    public class PutText : Transform<IplImage, IplImage>
    {
        [Description("Text string to be drawn.")]
        public string Text { get; set; }

        [Description("Bottom-left corner of the text string in the image.")]
        public Point Position { get; set; }

        [Description("Thickness of the lines used to draw a text.")]
        public int Thickness { get; set; }

        [Description("Scale factor that is multiplied by the font-specific base size.")]
        public double Scale { get; set; }

        [Description("Text color.")]
        public Scalar Color { get; set; }

        public PutText()
        {
            Text = "";
            Position = new Point(0, 0);
            Thickness = 1;
            Scale = 1.0;
            Color = Scalar.All(255);
        }

        public override IObservable<IplImage> Process(IObservable<IplImage> source)
        {
            return source.Select(input =>
            {
                var output = input.Clone();
                CV.PutText(output, Text, Position, new Font(Scale, Thickness), Color);
                return output;
            });
        }
    }
}
