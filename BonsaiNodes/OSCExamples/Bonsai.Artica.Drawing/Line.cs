﻿using Bonsai;
using OpenCV.Net;
using System;
using System.ComponentModel;
using System.Reactive.Linq;

namespace Bonsai.Artica.Drawing
{
    [Description("Draws a line segment connecting two points.")]
    public class Line : Transform<IplImage, IplImage>
    {
        [Description("First point of the line segment.")]
        public Point A { get; set; }

        [Description("Second point of the line segment.")]
        public Point B { get; set; }

        [Description("Line color.")]
        public Scalar Color { get; set; }

        [Description("Line thickness.")]
        public int Thickness { get; set; }

        public override IObservable<IplImage> Process(IObservable<IplImage> source)
        {
            return source.Select(input =>
            {
                var output = input.Clone();
                CV.Line(output, A, B, Color, Thickness);
                return output;
            });
        }
    }
}
