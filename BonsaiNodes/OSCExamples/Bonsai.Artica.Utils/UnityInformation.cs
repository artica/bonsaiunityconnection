﻿namespace Bonsai.Artica.Utils
{
    /// <summary>
    /// Class with the information that we want to send to our cube.
    /// </summary>
    public class UnityInformation
    {
        //Transform of the Cube.
        public float transformX { get; set; }
        public float transformY { get; set; }
        public float transformZ { get; set; }

        //Rotation of the Cube.
        public float rotationX { get; set; }
        public float rotationY { get; set; }
        public float rotationZ { get; set; }

        //Scale of the Cube.
        public float scaleX { get; set; }
        public float scaleY { get; set; }
        public float scaleZ { get; set; }

    }
}
