﻿using Bonsai.Vision;
using OpenCV.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bonsai.Artica.Utils
{
    public class DataForUnity : Source<UnityObj>
    {
        public override IObservable<KeyPointCollection> Process(IObservable<Tuple<Contour, IplImage>> source)
        {
            return source.Select(input =>
            {
                var contour = input.Item1;
                var image = input.Item2;
                var points = new KeyPointCollection(image);
                if (contour != null)
                {
                    var contourPoints = new Point[contour.Count];
                    contour.CopyTo(contourPoints);
                    for (int i = 0; i < contourPoints.Length; i++)
                    {
                        points.Add(new Point2f(contourPoints[i]));
                    }
                }

                return points;
            });
        }
    }
}
