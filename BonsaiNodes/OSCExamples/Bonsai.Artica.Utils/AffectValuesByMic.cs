﻿using System;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Xml.Serialization;

/// <summary>
/// This Class was created only to select what do we pretend to change in this demo.
/// We can use more parameters for this.
/// </summary>
namespace Bonsai.Artica.Utils
{
    public class AffectValuesByMic : Transform<Tuple<UnityInformation, double, double>, UnityInformation>
    {

        public bool changeTX { get; set; }
        public bool changeTY { get; set; }
        public bool changeTZ { get; set; }

        public bool changeRX { get; set; }
        public bool changeRY { get; set; }
        public bool changeRZ { get; set; }

        public bool changeSX { get; set; }
        public bool changeSY { get; set; }
        public bool changeSZ { get; set; }


        public override IObservable<UnityInformation> Process(IObservable<Tuple<UnityInformation, double, double>> source)
        {
            return source.Select(input =>
            {
                    var result = input.Item1;
                var micDB = input.Item2;
                var volumeDB = input.Item3;
                UnityInformationMessage message = new UnityInformationMessage();

                if(changeTX)
                    result.transformX = (float)(result.transformX + micDB);

                if(changeTY)
                    result.transformY = (float)(result.transformY + volumeDB);

                if(changeTZ)
                    result.transformZ = (float)(result.transformZ + micDB);

                if(changeRX)
                    result.rotationX = (float)(result.rotationX + micDB);

                if(changeRY)
                    result.rotationY = (float)(result.rotationY + micDB);

                if(changeRZ)
                    result.rotationZ = (float)(result.rotationZ + micDB);

                if(changeSX)
                    result.scaleX = (float)(result.scaleX + volumeDB * 10);

                if(changeSY)
                    result.scaleY = (float)(result.scaleY + volumeDB * 10);

                if(changeSZ)
                    result.scaleZ = (float)(result.scaleZ + volumeDB * 10);
                    
                
                return result;
            });
        }

 
    }
}
