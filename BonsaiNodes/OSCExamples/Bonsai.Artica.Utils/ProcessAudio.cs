﻿using Bonsai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.ComponentModel;

namespace Bonsai.Artica.Utils
{
    [Description("Generate an audio message according to a normalized peak value.")]
    public class ProcessAudio : Transform<short, double>
    {
        public int AudioTriggerVolume { get; set; }

        public double VolumeDb;

        public ProcessAudio()
        {
            AudioTriggerVolume = 70;
        }

        public override IObservable<double> Process(IObservable<short> source)
        {
            return source.Select(input =>
            {
                float normalizedInput = input / (float)short.MaxValue;
                VolumeDb = normalizedInput;
                return VolumeDb;
            });
        }

    }
}
