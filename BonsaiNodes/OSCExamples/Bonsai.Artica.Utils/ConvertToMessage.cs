﻿using System;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Xml.Serialization;

/// <summary>
/// This Class will create a Bonsai Node of type Transform that will apply alterations in this situation it will change
/// te UnityInformation in one XML string to be sent by OSC or TCP.
/// </summary>
namespace Bonsai.Artica.Utils
{
    public class ConvertToMessage : Transform<UnityInformation, string>
    {
        public override IObservable<string> Process(IObservable<UnityInformation> source)
        {
            return source.Select(input =>
            {
 
                    UnityInformationMessage message = new UnityInformationMessage();

                    message.TransformX = input.transformX;
                    message.TransformY = input.transformY;
                    message.TransformZ = input.transformZ;

                    message.RotationX = input.rotationX;
                    message.RotationY = input.rotationY;
                    message.RotationZ = input.rotationZ;

                    message.ScaleX = input.scaleX;
                    message.ScaleY = input.scaleY;
                    message.ScaleZ = input.scaleZ;

                    var messageXml = SerializeMessage(message);
                    
                
                return messageXml;
            });
        }

        /// <summary>
        /// Method responsable for serialize the information in one string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string SerializeMessage(UnityInformationMessage value)
        {

            if (value == null)
            {
                return null;
            }

            XmlSerializer serializer = new XmlSerializer(value.GetType());

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            StringBuilder builder = new StringBuilder();
            using (StringWriter textWriter = new EncodingStringWriter(builder, Encoding.ASCII))
            {
                serializer.Serialize(textWriter, value, ns);
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// Get the bytes from our string if we want to send the information in array of bytes.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }



    }
}
