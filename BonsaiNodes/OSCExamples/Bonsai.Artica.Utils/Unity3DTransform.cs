﻿using Bonsai.Vision;
using OpenCV.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;

/// <summary>
/// Class that will create a Node in Bonsai of the type Source that will be the one we are going to use to manipulate the
/// cube by changing it values.
/// </summary>

namespace Bonsai.Artica.Utils
{
    public class Unity3DTransform : Source<UnityInformation>
    {
        /// <summary>
        /// Vector that will apply transform to the unity cube.
        /// </summary>
        [Description("Vector that will apply transform to the unity cube.")]
        public Point3f Transform { get; set; }
        /// <summary>
        /// Vector that will apply the rotation to the cube with the values in Degrees.
        /// </summary>
        [Description("Vector that will apply the rotation to the cube with the values in Degrees.")]
        public Point3f Rotation { get; set; }
        /// <summary>
        /// Vector that will apply the scale of the cube being able to change his size.
        /// </summary>
        [Description("Vector that will apply the scale of the cube being able to change his size.")]
        public Point3f Scale { get; set; }

        /// <summary>
        /// This will generate one information as default like a constructor before it starts running the node. 
        /// </summary>
        /// <returns></returns>
        public override IObservable<UnityInformation> Generate()
        {
            return Observable.Defer(() => Observable.Return(CreateInfo()));
        }

        /// <summary>
        /// This Method will send the value to the others nodes by creating the info with the correct struct since we are not using
        /// OpenCV in Unity we can't send Point3f.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public IObservable<UnityInformation> Generate<TSource>(IObservable<TSource> source)
        {
            return source.Select(x => CreateInfo());
        }

        UnityInformation CreateInfo()
        {
            var uInfo = new UnityInformation();

            //Transform
            uInfo.transformX = Transform.X;
            uInfo.transformY = Transform.Y;
            uInfo.transformZ = Transform.Z;

            //Rotation
            uInfo.rotationX = Rotation.X;
            uInfo.rotationY = Rotation.Y;
            uInfo.rotationZ = Rotation.Z;

            //Scale
            uInfo.scaleX = Scale.X;
            uInfo.scaleY = Scale.Y;
            uInfo.scaleZ = Scale.Z;

            return uInfo;
        }


    }
}
