﻿using System.Xml.Serialization;
/// <summary>
/// Class Responsable to create the XML string to be send to unity.
/// </summary>

namespace Bonsai.Artica.Utils
{
    /// <summary>
    /// Class UnityInformationMessage.
    /// </summary>
    [XmlRoot("cubeStatsMessage")]
    public class UnityInformationMessage : IMessage
    {
        //Transform

        /// <summary>
        /// Variable that will contains the value of the X value in Transform parameter.
        /// </summary>
        private float transformX;
        /// <summary>
        /// Variable that will contains the value of the Y value in Transform parameter.
        /// </summary>
        private float transformY;
        /// <summary>
        /// Variable that will contains the value of the Z value in Transform parameter.
        /// </summary>
        private float transformZ;

        //Rotation

        /// <summary>
        /// Variable that will contains the value of the X value in Rotation parameter.
        /// </summary>
        private float rotationX;
        /// <summary>
        /// Variable that will contains the value of the Y value in Rotation parameter.
        /// </summary>
        private float rotationY;
        /// <summary>
        /// Variable that will contains the value of the Z value in Rotation parameter.
        /// </summary>
        private float rotationZ;

        //Scale

        /// <summary>
        /// Variable that will contains the value of the X value in Scale parameter.
        /// </summary>
        private float scaleX;
        /// <summary>
        /// Variable that will contains the value of the Y value in Scale parameter.
        /// </summary>
        private float scaleY;
        /// <summary>
        /// Variable that will contains the value of the Z value in Scale parameter.
        /// </summary>
        private float scaleZ;

        /// <summary>
        /// This will write the Transform X as atribute of XML.
        /// </summary>
        [XmlAttribute("transformX")]
        public float TransformX
        {
            get { return transformX; }
            set { transformX = value; }
        }

        /// <summary>
        /// This will write the Transform Y as atribute of XML.
        /// </summary>
        [XmlAttribute("transformY")]
        public float TransformY
        {
            get { return transformY; }
            set { transformY = value; }
        }

        /// <summary>
        /// This will write the Transform Z as atribute of XML.
        /// </summary>
        [XmlAttribute("transformZ")]
        public float TransformZ
        {
            get { return transformZ; }
            set { transformZ = value; }
        }

        /// <summary>
        /// This will write the Rotation X as atribute of XML.
        /// </summary>
        [XmlAttribute("rotationX")]
        public float RotationX
        {
            get { return rotationX; }
            set { rotationX = value; }
        }

        /// <summary>
        /// This will write the Rotation Y as atribute of XML.
        /// </summary>
        [XmlAttribute("rotationY")]
        public float RotationY
        {
            get { return rotationY; }
            set { rotationY = value; }
        }

        /// <summary>
        /// This will write the Rotation Z as atribute of XML.
        /// </summary>
        [XmlAttribute("rotationZ")]
        public float RotationZ
        {
            get { return rotationZ; }
            set { rotationZ = value; }
        }

        /// <summary>
        /// This will write the Scale X as atribute of XML.
        /// </summary>
        [XmlAttribute("scaleX")]
        public float ScaleX
        {
            get { return scaleX; }
            set { scaleX = value; }
        }

        /// <summary>
        /// This will write the Scale Y as atribute of XML.
        /// </summary>
        [XmlAttribute("scaleY")]
        public float ScaleY
        {
            get { return scaleY; }
            set { scaleY = value; }
        }

        /// <summary>
        /// This will write the Scale Z as atribute of XML.
        /// </summary>
        [XmlAttribute("scaleZ")]
        public float ScaleZ
        {
            get { return scaleZ; }
            set { scaleZ = value; }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            string TransformXStr = "TransformX: " + this.TransformX.ToString("n2");
            string TransformYStr = "TransformY: " + this.TransformY.ToString("n2");
            string TransformZStr = "TransformZ: " + this.TransformZ.ToString("n2");
            return "{TRANSFORM} - " + TransformXStr + " | " + TransformYStr + " | " + TransformZStr;
        }
    }
}
