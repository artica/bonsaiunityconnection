﻿using Bonsai;
using Bonsai.Vision;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.ComponentModel;
using System.Text;

using OpenCV.Net;
using System.Runtime.InteropServices;
using Bonsai.Artica.TSPS;

namespace Bonsai.Artica.Utils
{

    public class NormalizeCentroid : Transform<Tuple<TrackedComponent, Size>, TrackedComponent>
    {

      

        public override IObservable<TrackedComponent> Process(IObservable<Tuple<TrackedComponent, Size>> source)
        {
            return source.Select(input =>
            {

                var result = input.Item1;

                result.Centroid = new Point2f(result.Centroid.X / input.Item2.Width, result.Centroid.Y / input.Item2.Height);
         
                return result;
            });
        }
    }
}
