﻿using Bonsai;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reactive.Linq;

namespace Bonsai.Artica.TSPS
{
    [Description("Counting people passing by a Vertical line.")]
    public class PeopleCounting : Transform<BlobCollection, Tuple<int,int>>
    {
        [Description("X coordinate threshold for people entering.")]
        public int XEntering { get; set; }

        [Description("X coordinate threshold for people leaving.")]
        public int XLeaving { get; set; }

        [Description("Minimum Y coordinate.")]
        public int Ymin { get; set; }

        [Description("Maximum Y coordinate.")]
        public int Ymax { get; set; }

        public PeopleCounting()
        {
            XEntering = 800;
            XLeaving = 850;

            Ymin = 300;
            Ymax = 500;
        }

        public override IObservable<Tuple<int,int>> Process(IObservable<BlobCollection> source)
        {
            return Observable.Defer(() =>
            {
                Dictionary<int, Blob> trackedBlobs = null;
                Dictionary<int, int> blobEntered = null;
                Dictionary<int, int> blobLeft = null;
                return source.Select(input =>
                {
                    int entering = 0;
                    int leaving = 0;
                    trackedBlobs = trackedBlobs ?? new Dictionary<int, Blob>();
                    blobEntered = blobEntered ?? new Dictionary<int, int>();
                    blobLeft = blobLeft ?? new Dictionary<int, int>();
                    foreach (var blob in input)
                    {
                        if (trackedBlobs.ContainsKey(blob.ID) 
                            && trackedBlobs[blob.ID].Centroid.Y < Ymax && trackedBlobs[blob.ID].Centroid.Y > Ymin
                            && blob.Centroid.Y < Ymax && blob.Centroid.Y > Ymin)
                        {
                            if (trackedBlobs[blob.ID].Centroid.X > XEntering && blob.Centroid.X < XEntering && !blobEntered.ContainsKey(blob.ID))
                            {
                                entering++;
                                blobEntered[blob.ID] = 1;
                            }
                            else if (trackedBlobs[blob.ID].Centroid.X < XLeaving && blob.Centroid.X > XLeaving && !blobLeft.ContainsKey(blob.ID))
                            {
                                leaving++;
                                blobLeft[blob.ID] = 1;
                            }                           
                            trackedBlobs[blob.ID].Update(blob);                        
                        }
                        else
                            trackedBlobs[blob.ID] = new Blob(blob);
                    }
                    return new Tuple<int, int>(entering, leaving);
                });
            });
        }
    }
}
