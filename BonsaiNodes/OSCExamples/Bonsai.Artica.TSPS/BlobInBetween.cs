﻿using OpenCV.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bonsai.Artica.TSPS
{
    [Description("Check if blob is between two values.")]
    public class BlobInBetween : Transform<Point2f, bool>
    {
        [Description("Minimum X coordinate.")]
        public float Xmin { get; set; }

        [Description("Maximum X coordinate.")]
        public float Xmax { get; set; }

        [Description("Minimum Y coordinate.")]
        public float Ymin { get; set; }

        [Description("Maximum Y coordinate.")]
        public float Ymax { get; set; }

        public BlobInBetween()
        {
            Xmin = 800f;
            Xmax = 850f;

            Ymin = 300f;
            Ymax = 500f;
        }

        public override IObservable<bool> Process(IObservable<Point2f> source)
        {
            return Observable.Defer(() =>
            {

                return source.Select(input =>
                {
                    
                    bool result = false;

                    result = (input.X > Xmin && input.X < Xmax) && (input.Y > Ymin && input.Y < Ymax);

                    return result;
                });
            });
        }
    }
}

