﻿using Bonsai.Vision;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bonsai.Artica.TSPS
{
    public class TakeLargest : Transform<TrackedComponentCollection, TrackedComponentCollection>
    {
        public int Count { get; set; }

        public override IObservable<TrackedComponentCollection> Process(IObservable<TrackedComponentCollection> source)
        {
            return source.Select(input =>
            {
                var components = input.OrderByDescending(xs => xs.Area).Take(Count);
                var result = new TrackedComponentCollection(input.ImageSize);
                foreach (var component in components)
                {
                    result.Add(component);
                }
                return result;
            });
        }
    }
}
