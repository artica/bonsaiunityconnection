﻿using Bonsai;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reactive.Linq;

namespace Bonsai.Artica.TSPS
{
    [Description("Write to CSV file when people enter/leave.")]
    public class PeopleCountingWriter : Sink<Tuple<int,int>>
    {
        [Description("Path where to save the file to.")]
        public string Path { get; set; }

        public PeopleCountingWriter()
        {
            Path = "";
        }

        public override IObservable<Tuple<int,int>> Process(IObservable<Tuple<int,int>> source)
        {
            return source.Do(input =>
            {
               if(input.Item1 > 0 || input.Item2 > 0)
                   WriteToCsv(input.Item1, input.Item2);
            });
        }

        private void WriteToCsv(int nbEntering, int nbLeaving)
        {
            var date = DateTime.Now.ToString("yyMMdd", CultureInfo.InvariantCulture);
            var filename = Path + "\\" + "logs_io_" + date + ".csv";
            try
            {
                using (var writer = new CsvFileWriter(filename, true))
                {
                    writer.Delimiter = ';';
                    var columns = new List<string>();
                    columns.Add(DateTime.Now.ToString());
                    columns.Add(nbEntering.ToString());
                    columns.Add(nbLeaving.ToString());
                    writer.WriteRow(columns);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not write to CSV : " + ex.Message);
            }
        }
    }
}
