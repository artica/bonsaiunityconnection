﻿using Bonsai.Vision;
using OpenCV.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bonsai.Artica.TSPS
{
    public class TrackedComponent : ConnectedComponent
    {
        public TrackedComponent()
        {
            Id = -1;
        }

        public int Id { get; set; }

        public Point2f Velocity { get; set; }

        public DateTime LastSighting { get; set; }

        public DateTime DateCreated { get; set; }

        public TrackedComponentState State { get; set; }

        public static TrackedComponent FromTrackedComponent(TrackedComponent component)
        {
            var result = FromConnectedComponent(component, component.Id);
            result.DateCreated = component.DateCreated;
            result.LastSighting = component.LastSighting;
            return result;
        }

        public static TrackedComponent FromConnectedComponent(ConnectedComponent component, int id = -1)
        {
            var result = new TrackedComponent();
            result.Area = component.Area;
            result.Centroid = component.Centroid;
            result.Contour = component.Contour;
            result.MajorAxisLength = component.MajorAxisLength;
            result.MinorAxisLength = component.MinorAxisLength;
            result.Orientation = component.Orientation;
            result.Patch = component.Patch;
            result.Id = id;
            return result;
        }
    }
}
