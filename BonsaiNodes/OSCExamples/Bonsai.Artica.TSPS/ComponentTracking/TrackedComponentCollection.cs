﻿using OpenCV.Net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bonsai.Artica.TSPS
{
    public class TrackedComponentCollection : KeyedCollection<int, TrackedComponent>
    {
        public TrackedComponentCollection(Size imageSize)
        {
            ImageSize = imageSize;
        }

        public Size ImageSize { get; private set; }

        protected override int GetKeyForItem(TrackedComponent item)
        {
            return item.Id;
        }
    }
}
