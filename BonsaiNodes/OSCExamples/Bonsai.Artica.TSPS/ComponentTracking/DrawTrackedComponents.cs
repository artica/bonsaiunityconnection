﻿using Bonsai;
using OpenCV.Net;
using System;
using System.ComponentModel;
using System.Reactive.Linq;

namespace Bonsai.Artica.TSPS
{
    [Description("Draws the bounding box of the input contours.")]
    public class DrawTrackedComponents : Transform<Tuple<TrackedComponentCollection, IplImage>, IplImage>
    {
        static readonly Scalar[] ColorMap = GenerateColorMap(10);

        static Scalar[] GenerateColorMap(int count)
        {
            var contrast = 255;
            var colorMap = new Scalar[count];
            for (int i = 0; i < colorMap.Length; i++)
            {
                var hue = 6.0 * i / colorMap.Length;
                var intensity = 255 * (hue % 1);
                var sextant = (int)Math.Floor(hue) % 6;
                switch (sextant)
                {
                    case 0: colorMap[i] = Scalar.Rgb(contrast, intensity, 0); break;
                    case 1: colorMap[i] = Scalar.Rgb(intensity, contrast, 0); break;
                    case 2: colorMap[i] = Scalar.Rgb(0, contrast, intensity); break;
                    case 3: colorMap[i] = Scalar.Rgb(0, intensity, contrast); break;
                    case 4: colorMap[i] = Scalar.Rgb(intensity, 0, contrast); break;
                    case 5: colorMap[i] = Scalar.Rgb(contrast, 0, intensity); break;
                }
            }
            return colorMap;
        }

        public override IObservable<IplImage> Process(IObservable<Tuple<TrackedComponentCollection, IplImage>> source)
        {
            return source.Select(input =>
            {
                var output = input.Item2.Clone();

                foreach (var blob in input.Item1)
                {
                    if (blob.State < TrackedComponentState.Missing)
                    {
                        CV.Rectangle(output, blob.Contour.Rect, ColorMap[blob.Id % ColorMap.Length], 2);
                    }
                }
                return output;
            });
        }
    }
}
