﻿using Bonsai.Vision;
using OpenCV.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bonsai.Artica.TSPS
{
    public class ComponentTracking : Transform<ConnectedComponentCollection, TrackedComponentCollection>
    {
        public float MaxDistance { get; set; }

        public TimeSpan MissingDueTime { get; set; }

        public override IObservable<TrackedComponentCollection> Process(IObservable<ConnectedComponentCollection> source)
        {
            return Observable.Defer(() =>
            {
                var nextId = 0;
                TrackedComponentCollection history = null;
                var unassigned = new List<ConnectedComponent>();
                var assignments = new Dictionary<int, Assignment>();
                return source.Select(input =>
                {
                    unassigned.Clear();
                    assignments.Clear();
                    var currentTime = DateTime.Now;
                    var maxDistanceSquared = MaxDistance * MaxDistance;
                    var result = new TrackedComponentCollection(input.ImageSize);
                    if (history != null)
                    {
                        foreach (var component in input)
                        {
                            var bestDistanceSquared = float.MaxValue;
                            var bestMatch = default(TrackedComponent);
                            var bestVelocity = Point2f.Zero;

                            foreach (var candidate in history)
                            {
                                if (candidate.State == TrackedComponentState.Exit) continue;
                                var velocity = component.Centroid - candidate.Centroid;
                                var distanceSquared = velocity.X * velocity.X + velocity.Y * velocity.Y;
                                if (bestMatch == null || bestDistanceSquared > distanceSquared)
                                {
                                    bestDistanceSquared = distanceSquared;
                                    bestMatch = candidate;
                                    bestVelocity = velocity;
                                }
                            }

                            // a tracking assignment is generated if there is a best match
                            // AND that best match is better than existing assignments
                            if (bestMatch != null &&
                                bestDistanceSquared <= maxDistanceSquared &&
                                (!assignments.TryGetValue(bestMatch.Id, out Assignment bestAssignment) ||
                                 bestDistanceSquared < bestAssignment.DistanceSquared))
                            {
                                var tracked = TrackedComponent.FromConnectedComponent(component, bestMatch.Id);
                                assignments[tracked.Id] = new Assignment(tracked, bestDistanceSquared);
                                tracked.DateCreated = bestMatch.DateCreated;
                                tracked.LastSighting = currentTime;
                                tracked.Velocity = bestVelocity;
                                tracked.State = TrackedComponentState.Active;
                            }
                            else unassigned.Add(component);
                        }

                        foreach (var component in history)
                        {
                            // assign updated components
                            if (assignments.TryGetValue(component.Id, out Assignment assignment))
                            {
                                result.Add(assignment.Component);
                            }
                            // handle missing components
                            else
                            {
                                if (component.State == TrackedComponentState.Exit) continue;
                                var tracked = TrackedComponent.FromTrackedComponent(component);
                                var interval = currentTime - component.LastSighting;
                                tracked.State = interval >= MissingDueTime
                                    ? TrackedComponentState.Exit
                                    : TrackedComponentState.Missing;
                                result.Add(tracked);
                            }
                        }
                    }
                    else unassigned.AddRange(input);

                    // add new components
                    foreach (var component in unassigned)
                    {
                        var tracked = TrackedComponent.FromConnectedComponent(component, nextId++);
                        tracked.DateCreated = currentTime;
                        tracked.LastSighting = currentTime;
                        tracked.State = TrackedComponentState.Enter;
                        result.Add(tracked);
                    }
                    
                    return history = result;
                });
            });
        }

        struct Assignment
        {
            public TrackedComponent Component;
            public float DistanceSquared;

            public Assignment(TrackedComponent component, float distanceSquared)
            {
                Component = component;
                DistanceSquared = distanceSquared;
            }
        }
    }
}
