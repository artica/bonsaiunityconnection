﻿using Bonsai;
using Bonsai.Vision;
using OpenCV.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;


namespace Bonsai.Artica.TSPS
{
    [Description("Custom blob tracker.")]
    public class BlobTracking : Transform<Tuple<IplImage, ConnectedComponentCollection>, Tuple<IplImage, BlobCollection>>
    {
        [Description("Unique Camera Identifier.")]
        public int CameraID { get; set; }

        [Description("The amount of seconds to lose tracking of a blob before it is purged.")]
        public int MaxBlobTrackLossTime { get; set; }

        [Description("The maximum distance to match blobs between 2 frames in pixels.")]
        public float MaxDistance { get; set; }

        [Description("Draw bounding box and blob ID on the output image.")]
        public bool DrawFeedback { get; set; }

        [Description("Minimum velocity a blob should have to NOT be considered as background.")]
        public float MinSpeed { get; set; }

        //[Description("Maximum time a blob can stay static without being considered as background.")]
        //public int MaxBlobStaticTime { get; set; }

        //[Description("Maximum time a blob can stay static without being considered as background.")]       
        //public int MinBlobHistoricCount { get; set; }

        //[Description("Interval to reset the ignore list.")]
        //public int ResetIgnoredBlobsIntervalMinutes { get; set; }

        public BlobTracking()
        {
            MaxBlobTrackLossTime = 2;
            MaxDistance = 100;
            DrawFeedback = true;
            MinSpeed = 0;
            //MaxBlobStaticTime = 30;
            //ResetIgnoredBlobsIntervalMinutes = 60;
            //MinBlobHistoricCount = 0;
        }

        public override IObservable<Tuple<IplImage, BlobCollection>> Process(IObservable<Tuple<IplImage, ConnectedComponentCollection>> source)
        {
            return Observable.Defer(() =>
           {
               Dictionary<int, DateTime> trackedBlobsLastMoving = null;
               Dictionary<int, Blob> trackedBlobs = null;
               Dictionary<int, DateTime> trackedBlobsLastSighting = null;
               Dictionary<int, int> trackedBlobsHistoric = null;
               //List<int> trackedBlobIgnored = null;
               int newID = 0;

               object mutex = new object();

               //var timer = Observable.Timer(TimeSpan.Zero, TimeSpan.FromSeconds(20))
               //.Subscribe(x =>
               //{
               //    lock (mutex)
               //    {
               //        if (trackedBlobsLastMoving == null || trackedBlobIgnored == null)
               //            return;

               //        trackedBlobIgnored.Clear();
               //        var keys = new List<int>(trackedBlobsLastMoving.Keys);
               //        foreach (var key in keys)
               //        {
               //            trackedBlobsLastMoving[key] = DateTime.Now;
               //        }
               //    }
               //});
               
               return source.Select(input =>
               {
                   trackedBlobs = trackedBlobs ?? new Dictionary<int, Blob>();
                   trackedBlobsLastSighting = trackedBlobsLastSighting ?? new Dictionary<int, DateTime>();
                   trackedBlobsLastMoving = trackedBlobsLastMoving ?? new Dictionary<int, DateTime>();
                   //trackedBlobIgnored = trackedBlobIgnored ?? new List<int>();
                   trackedBlobsHistoric = trackedBlobsHistoric ?? new Dictionary<int, int>();
                   var blobUpdateMap = new Dictionary<int, List<Blob>>();

                   foreach (var blob in trackedBlobs.Values.Where(blob => blob.State != BlobState.Left))
                   {
                       blob.State = BlobState.Updated;
                   }

                   //1. Assign found blob to either new key (if there was no blob near that area previously)
                   //   or update the key
                   var collection = new BlobCollection(input.Item1.Size);
                   foreach (var blob in input.Item2)
                       collection.Add(new Blob(blob) { Centroid = new Point2f(blob.Centroid.X / input.Item1.Width, blob.Centroid.Y / input.Item1.Height) });

                   var maxDist = MaxDistance * MaxDistance;
                   foreach (Blob newBlob in collection)
                   {
                       //A) Found closest blob :
                       float closestSqDistance = float.MaxValue;
                       Blob closestBlob = null;

                       foreach (var blob in trackedBlobs.Values)
                       {
                           float xDiff = newBlob.Centroid.X - blob.Centroid.X;
                           float yDiff = newBlob.Centroid.Y - blob.Centroid.Y;
                           float sqDist = (xDiff * xDiff) + (yDiff * yDiff);
                           if (sqDist < closestSqDistance)
                           {
                               closestSqDistance = sqDist;
                               closestBlob = blob;
                           }
                       }

                       //B) Add blob to tracked list or update tracked blob                       
                       if (closestBlob == null || closestSqDistance > maxDist)
                       {
                           //There is no blob matching this new blob, add it as a new one.
                           newBlob.CameraID = CameraID;
                           newBlob.ID = newID;
                           trackedBlobs[newID] = new Blob(newBlob) { State = BlobState.Entered };
                           trackedBlobsLastSighting[newID] = DateTime.Now;
                           trackedBlobsHistoric[newID] = 0;
                           lock (mutex)
                           {
                               trackedBlobsLastMoving[newID] = DateTime.Now;
                           }
                           newID++;
                       }
                       else
                       {
                           if (!blobUpdateMap.ContainsKey(closestBlob.ID))
                           {
                               blobUpdateMap[closestBlob.ID] = new List<Blob>();
                           }
                           blobUpdateMap[closestBlob.ID].Add(newBlob);
                       }
                   }

                   //2. For IDs that were tracked (i.e. were previously detected and were still detected now), update them
                   foreach (var idAndBlobList in blobUpdateMap)
                   {
                       if (idAndBlobList.Value.Count == 1)
                       {
                           //There was only one face in the new frame that mapped to this ID
                           int id = idAndBlobList.Key;

                           idAndBlobList.Value[0].ID = id;
                           idAndBlobList.Value[0].CameraID = CameraID;

                           trackedBlobs[id].Update(idAndBlobList.Value[0]);
                           trackedBlobsLastSighting[id] = DateTime.Now;
                           trackedBlobsHistoric[id]++;
                           lock (mutex)
                           {
                               var velocity = trackedBlobs[id].Velocity;
                               var speed = Math.Sqrt(velocity.X * velocity.X + velocity.Y * velocity.Y);
                               if (speed > MinSpeed)
                                   trackedBlobsLastMoving[id] = DateTime.Now;
                           }
                       }
                       else
                       {
                           //Update the matched blob with the average blob         
                           int id = idAndBlobList.Key;
                           var avg = AverageBlobs(idAndBlobList.Value.ToList(), id);
                           avg.Contour = trackedBlobs[id].Contour;//TODO: MERGE CONTOURS INSTEAD.

                           trackedBlobs[id].Update(avg);
                           trackedBlobsLastSighting[id] = DateTime.Now;
                           trackedBlobsHistoric[id]++;
                           lock (mutex)
                           {
                               var velocity = trackedBlobs[id].Velocity;
                               var speed = Math.Sqrt(velocity.X * velocity.X + velocity.Y * velocity.Y);
                               if (speed > MinSpeed)
                                   trackedBlobsLastMoving[id] = DateTime.Now;
                           }
                       }
                   }

                   //3. Remove old blobs
                   var threshold = new TimeSpan(0, 0, MaxBlobTrackLossTime);
                   var blobsToRemove = trackedBlobsLastSighting.Where((timeEntry) => { return (DateTime.Now - timeEntry.Value) > threshold; }).ToList();
                   foreach (var b in blobsToRemove)
                   {
                       var blob = trackedBlobs[b.Key];
                       if (blob.State == BlobState.Left)
                       {
                           trackedBlobs.Remove(b.Key);
                           trackedBlobsLastSighting.Remove(b.Key);
                       }
                       else blob.State = BlobState.Left;
                   }

                   //var listOK = new List<Blob>();
                   //lock (mutex)
                   //{
                   //    //3.5. Ignore static blobs
                   //    threshold = new TimeSpan(0, 0, MaxBlobStaticTime);                       
                   //    var blobsToIgnore = trackedBlobsLastMoving.Where((timeEntry) => { return (DateTime.Now - timeEntry.Value) > threshold && !trackedBlobIgnored.Contains(timeEntry.Key); }).ToList();
                   //    foreach (var b in blobsToIgnore)
                   //    {
                   //        trackedBlobIgnored.Add(b.Key);
                   //    }
                   //    listOK = trackedBlobs.Values.ToList().Where(b => !trackedBlobIgnored.Contains(b.ID) && trackedBlobsHistoric[b.ID] > MinBlobHistoricCount).OrderBy(b => b.ID).ToList();
                   //}

                   var listOK = trackedBlobs.Values.OrderBy(b => b.ID).ToList();

                   //4. Merge blobs with overlapping bounding boxes.                  
                   //for (int i = 0; i < listOK.Count; i++)
                   //{
                   //    for (int j = 0; j < listOK.Count; j++)
                   //    {
                   //        if (i == j)
                   //            continue;

                   //        var p1 = new Point(listOK[i].Contour.Rect.X, listOK[i].Contour.Rect.Y);
                   //        var p2 = new Point(listOK[j].Contour.Rect.X, listOK[j].Contour.Rect.Y);
                   //        var s1 = new Size(listOK[i].Contour.Rect.Width, listOK[i].Contour.Rect.Height);
                   //        var s2 = new Size(listOK[j].Contour.Rect.Width, listOK[j].Contour.Rect.Height);
                   //        var box = new Rect();
                   //        if (overlapRoi(p1, p2, s1, s2, ref box))
                   //        {
                   //            int keepId = listOK[i].Area > listOK[j].Area ? listOK[i].ID : listOK[j].ID;
                   //            int deleteId = listOK[i].Area > listOK[j].Area ? listOK[j].ID : listOK[i].ID;
                   //            var avg = AverageBlobs(listOK[i], listOK[j], keepId);
                   //            avg.Contour = listOK[i].Area > listOK[j].Area ? listOK[i].Contour : listOK[j].Contour;
                   //            listOK[listOK[i].Area > listOK[j].Area ? i : j] = avg;
                   //            listOK.RemoveAt(listOK[i].Area > listOK[j].Area ? j : i);

                   //            trackedBlobs[keepId] = avg;
                   //            trackedBlobs.Remove(deleteId);
                   //            trackedBlobsLastMoving.Remove(deleteId);
                   //            trackedBlobsLastSighting.Remove(deleteId);
                   //            trackedBlobIgnored.Remove(deleteId);
                   //            Console.WriteLine("Merged " + keepId + " with " + deleteId);
                   //            break;
                   //        }
                   //    }
                   //}

                   //4. Return result
                   var output = input.Item1.Clone();

                   if (DrawFeedback)
                   {
                       foreach (var blob in listOK)
                       {
                           //CV.DrawContours(output, blob.Contour, Scalar.All(255), Scalar.All(255), 0, 2);
                           CV.Rectangle(output, blob.Contour.Rect, Scalar.All(127), 2);
                           Point center = new Point((int)blob.Centroid.X, (int)blob.Centroid.Y);
                           CV.PutText(output, blob.ID.ToString(), center, new Font(10, 10), Scalar.All(127));
                           //CV.PutText(output, blob.Velocity.ToString("0.0"), new Point(center.X, center.Y + 20), new Font(2, 2), Scalar.All(255));
                       }
                   }

                   var blobs = new BlobCollection(listOK, input.Item1.Size, CameraID);

                   return new Tuple<IplImage, BlobCollection>(output, blobs);
               });
           });
        }

        //Rect MergeRects(List<Point> tls, List<int> widths, List<int> heights)
        //{
        //    if(tls.Count != widths.Count || tls.Count != heights.Count)
        //        return new Rect();

        //    int x_tl = tls.Min(p => p.X);
        //    int y_tl = tls.Min(p => p.Y);
        //    int x_br_min = int.MaxValue;
        //    for (int i = 0; i < widths.Count; i++)
        //    {
        //        if (tls[i].X + widths[i] < x_br_min)
        //            x_br_min = tls[i].X + widths[i];
        //    }
        //    int x_br = Math.Min(tl1.X + sz1.Width, tl2.X + sz2.Width);
        //    int y_br = Math.Min(tl1.Y + sz1.Height, tl2.Y + sz2.Height);
        //    return new Rect(x_tl, y_tl, x_br - x_tl, y_br - y_tl);
        //}

        bool overlapRoi(Point tl1, Point tl2, Size sz1, Size sz2, ref Rect roi)
        {
            int x_tl = Math.Max(tl1.X, tl2.X);
            int y_tl = Math.Max(tl1.Y, tl2.Y);
            int x_br = Math.Min(tl1.X + sz1.Width, tl2.X + sz2.Width);
            int y_br = Math.Min(tl1.Y + sz1.Height, tl2.Y + sz2.Height);
            if (x_tl < x_br && y_tl < y_br)
            {
                roi = new Rect(x_tl, y_tl, x_br - x_tl, y_br - y_tl);
                return true;
            }
            return false;
        }

        private bool pointInside(Point2f p, Rect c)
        {
            var p1 = new Point2f(c.X, c.Y);
            var p2 = new Point2f(c.X + c.Width, c.Y);
            var p3 = new Point2f(c.X + c.Width, c.Y + c.Height);
            var p4 = new Point2f(c.X, c.Y + c.Height);

            var pTest = new Point2f(p.X - p1.X, p.Y - p1.Y);

            var ratioW = pTest.X * (p2.X - p1.X) / c.Width + pTest.Y * (p2.Y - p1.Y) / c.Height;
            var ratioH = pTest.X * (p4.X - p1.X) / c.Width + pTest.Y * (p4.Y - p1.Y) / c.Height;

            return ratioW >= 0 && ratioW <= c.Width && ratioH >= 0 && ratioH <= c.Height;
        }

        private Blob AverageBlobs(Blob b1, Blob b2, int id)
        {
            var list = new List<Blob>();
            list.Add(b1);
            list.Add(b2);
            return AverageBlobs(list, id);
        }

        private Blob AverageBlobs(List<Blob> blobs, int id)
        {
            Blob avg = new Blob();
            avg.ID = id;
            avg.CameraID = CameraID;

            avg.Velocity = new Point2f(blobs.Average(b => b.Velocity.X), blobs.Average(b => b.Velocity.Y));

            avg.Area = blobs.Average(b => b.Area);
            avg.Centroid = new Point2f(blobs.Average(b => b.Centroid.X), blobs.Average(b => b.Centroid.Y));

            //Merge Contours : 
            //var contours = blobs.Select(b => b.Contour.ToArray<Point>());
            //var count = contours.Sum(c => c.Length);

            //var concat = new Point[count];
            //int dst = 0;
            //foreach (var array in contours)
            //{
            //    Array.Copy(array, 0, concat, dst, array.Length);
            //    dst += array.Length;
            //}
            //var memStorage = new MemStorage(count);
            //var seq = new Seq(SequenceElementType.Point, memStorage);
            //avg.Contour = Contour.FromSeq(seq);

            //try
            //{
            //    avg.Contour.Push(concat);

            //    Seq convexHull = null;
            //    //Seq convexityDefects = null;
            //    if (avg.Contour != null)
            //    {
            //        //var convexHullIndices = CV.ConvexHull2(avg.Contour);
            //        convexHull = CV.ConvexHull2(avg.Contour, returnPoints: true);
            //        //convexityDefects = CV.ConvexityDefects(avg.Contour, convexHullIndices);
            //    }
            //    avg.Contour = Contour.FromSeq(convexHull);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}

            avg.MajorAxisLength = blobs.Average(b => b.MajorAxisLength);
            avg.MinorAxisLength = blobs.Average(b => b.MinorAxisLength);
            avg.Orientation = blobs.Average(b => b.Orientation);
            avg.LastSighting = DateTime.Now;

            //Compute englobing bounding box :
            var rects = blobs.Select(b => b.ProjectedBoundingBox);
            int xmin = rects.Min(r => r.X);
            int xmax = rects.Max(r => r.X + r.Width);
            int ymin = rects.Min(r => r.Y);
            int ymax = rects.Max(r => r.Y + r.Height);
            avg.ProjectedBoundingBox = new Rect(xmin, ymin, xmax - xmin, ymax - ymin);
            return avg;
        }
    }
}
