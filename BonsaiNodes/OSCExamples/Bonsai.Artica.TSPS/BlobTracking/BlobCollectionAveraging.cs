﻿using Bonsai;
using OpenCV.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;

namespace Bonsai.Artica.TSPS
{
    [Description("Average a list of blob collection over time.")]
    public class BlobCollectionAveraging : Transform<IList<BlobCollection>, IList<BlobCollection>>
    {
        protected Dictionary<int, Dictionary<int, Blob>> trackedBlobs = null;

        public override IObservable<IList<BlobCollection>> Process(IObservable<IList<BlobCollection>> source)
        {
            return Observable.Defer(() =>
            {
                trackedBlobs = null;
                return source.Select(input =>
                {
                    trackedBlobs = trackedBlobs ?? Init(input);

                    foreach (var collection in input)
                        foreach (var blob in collection)
                        {
                            if (trackedBlobs[blob.CameraID].ContainsKey(blob.ID))
                            {
                                AverageBlob(blob);
                            }
                            else
                            {
                                trackedBlobs[blob.CameraID][blob.ID] = blob;
                            }
                        }

                    for (int i = 0; i < input.Count; i++)
                    {
                        var newList = input[i].ToList();
                        HashSet<int> newIDs = new HashSet<int>(newList.Select(b => b.ID));
                        var trackedList = trackedBlobs[input[i].CameraID].Values.ToList();
                        var toRemove = trackedList.Where(b => !newIDs.Contains(b.ID));
                        foreach (var blob in toRemove)
                            trackedBlobs[input[i].CameraID].Remove(blob.ID);
                    }

                    //TODO: Clean this.
                    var listInput = input as List<BlobCollection>;
                    var output = new List<BlobCollection>();
                    int id = 0;
                    foreach (var pair in trackedBlobs.Values)
                        output.Add(new BlobCollection(pair.Values.ToList(), listInput.FirstOrDefault().ImageSize, id++));
                    return output;
                });
            });
        }

        private Dictionary<int, Dictionary<int, Blob>> Init(IList<BlobCollection> collections)
        {
            var output = new Dictionary<int, Dictionary<int, Blob>>();
            foreach (var collection in collections)
                output[collection.CameraID] = new Dictionary<int, Blob>();
            return output;
        }

        private void AverageBlob(Blob b)
        {
            var avg = trackedBlobs[b.CameraID][b.ID];
            var dx = (avg.Velocity.X + b.Velocity.X) / 2.0f;
            var dy = (avg.Velocity.Y + b.Velocity.Y) / 2.0f;
            avg.Velocity = new Point2f(dx, dy);

            avg.Area = (avg.Area + b.Area) / 2.0f;
            avg.Centroid = new Point2f((avg.Centroid.X + b.Centroid.X) / 2, (int)(avg.Centroid.Y + b.Centroid.Y) / 2);
            //Too slow.
            //var array = b.Contour.ToArray<Point>();
            //avg.Contour.Push(array);
            //TODO: Merge contours.
            avg.Contour = b.Contour;

            avg.MajorAxisLength = (avg.MajorAxisLength + b.MajorAxisLength) / 2.0f;
            avg.MinorAxisLength = (avg.MinorAxisLength + b.MinorAxisLength) / 2.0f;
            avg.Orientation = (avg.Orientation + b.Orientation) / 2.0f;
            avg.LastSighting = b.LastSighting;
        }
    }
}
