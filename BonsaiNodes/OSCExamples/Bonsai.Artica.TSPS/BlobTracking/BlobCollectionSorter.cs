﻿using Bonsai;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Text;

namespace Bonsai.Artica.TSPS
{
    [Description("Filter out blobs not included within the FromDate to ToDate range.")]
    public class BlobCollectionSorter : Transform<BlobCollection, BlobCollection>
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public BlobCollectionSorter()
        {
            FromDate = DateTime.Now.AddMonths(-1);
            ToDate = DateTime.Now;
        }

        public override IObservable<BlobCollection> Process(IObservable<BlobCollection> source)
        {
            return source.Select(input =>
            {
                var output = new BlobCollection(input.ImageSize);
                foreach (var blob in input)
                    if (blob.LastSighting > FromDate && blob.LastSighting < ToDate)
                        output.Add(blob);
                return output;
            });
        }
    }
}
