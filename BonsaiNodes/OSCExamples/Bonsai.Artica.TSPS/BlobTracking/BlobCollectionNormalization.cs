﻿using Bonsai;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reactive.Linq;
using OpenCV.Net;

namespace Bonsai.Artica.TSPS
{
    [Description("Normalizer blob collection centroids between 0 and 1.")]
    public class BlobCollectionNormalization : Transform<BlobCollection, BlobCollection>
    {
        public override IObservable<BlobCollection> Process(IObservable<BlobCollection> source)
        {
            return source.Select(input =>
            {
                var output = new BlobCollection(input);
                foreach (var blob in output)
                {
                    blob.Centroid = new Point2f(blob.Centroid.X / output.ImageSize.Width, blob.Centroid.Y / output.ImageSize.Height);
                }
                return output;
            });
        }
    }
}
