﻿using Bonsai;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reactive.Linq;

namespace Bonsai.Artica.TSPS
{
    [Description("Format blob collections to an OSC message string.")]
    public class BlobCollectionToString : Transform<IList<BlobCollection>, string>
    {       
        public override IObservable<string> Process(IObservable<IList<BlobCollection>> source)
        {
            return source.Select(input =>
            {
                return CreateMessage(input);
            });
        }

        static string CreateMessage(IList<BlobCollection> input)
        {
            string output = input.Count > 0 ? "#" : "";
            foreach (var collection in input)
                foreach (var blob in collection)
                {
                    output += blob.CameraID + "|";
                    output += blob.ID + "|";
                    output += ((int)blob.Centroid.X).ToString() + "|";
                    output += ((int)blob.Centroid.Y).ToString() + "|";
                    output += blob.ProjectedBoundingBox.X + "|";
                    output += blob.ProjectedBoundingBox.Y + "|";
                    output += blob.ProjectedBoundingBox.X + blob.ProjectedBoundingBox.Width + "|";
                    output += blob.ProjectedBoundingBox.Y + blob.ProjectedBoundingBox.Height + "#";
                }
            return output;
        }
    }
}
