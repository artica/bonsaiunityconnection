﻿using Bonsai;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reactive.Linq;

namespace Bonsai.Artica.TSPS
{
    [Description("Save blob collections to a CSV file")]
    public class BlobCollectionWriter : Sink<IList<BlobCollection>>
    {
        [Description("Path where to save the file to.")]
        public string Path { get; set; }

        public BlobCollectionWriter()
        {
            Path = "";
        }

        public override IObservable<IList<BlobCollection>> Process(IObservable<IList<BlobCollection>> source)
        {
            return source.Select(input =>
            {
                foreach (var collection in input)
                    WriteToCsv(collection);
                return input;
            });
        }

        private void WriteToCsv(BlobCollection input)
        {
            var date = DateTime.Now.ToString("yyMMdd", CultureInfo.InvariantCulture);
            var filename = Path + "\\" + "logs_" + date + ".csv";
            try
            {
                using (var writer = new CsvFileWriter(filename, true))
                {
                    writer.Delimiter = ';';
                    foreach (var blob in input)
                    {
                        var columns = new List<string>();
                        columns.Add(blob.CameraID.ToString());
                        columns.Add(blob.ID.ToString());
                        columns.Add(DateTime.Now.ToString());
                        columns.Add(blob.Centroid.X.ToString());
                        columns.Add(blob.Centroid.Y.ToString());
                        writer.WriteRow(columns);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not write to CSV : " + ex.Message);
            }
        }
    }
}
