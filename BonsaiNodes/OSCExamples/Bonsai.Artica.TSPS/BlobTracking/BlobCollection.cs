﻿using Bonsai.Vision;
using OpenCV.Net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Bonsai.Artica.TSPS
{
    public enum BlobState
    {
        Entered,
        Updated,
        Left
    }

    public class Blob : ConnectedComponent
    {
        public int CameraID { get; set; }
        public int ID { get; set; }
        public Point2f Velocity { get; set; }
        public DateTime LastSighting { get; set; }
        public DateTime DateCreated { get; set; }
        public Rect ProjectedBoundingBox { get; set; }
        public float Depth { get; set; }

        public BlobState State { get; set; }

        public int Age
        {
            get { return (int)(LastSighting - DateCreated).TotalSeconds; }
        }

        public Blob()
            : base()
        {
            this.CameraID = -1;
            this.ID = -1;
            this.Velocity = Point2f.Zero;
            this.LastSighting = DateCreated = DateTime.Now;
            this.ProjectedBoundingBox = new Rect();
            this.State = BlobState.Entered;
        }

        public Blob(Blob blob)
        {
            this.CameraID = blob.CameraID;
            this.ID = blob.ID;

            this.Velocity = Point2f.Zero;
            this.Area = blob.Area;
            this.Centroid = blob.Centroid;
            this.Contour = blob.Contour;
            this.MajorAxisLength = blob.MajorAxisLength;
            this.MinorAxisLength = blob.MinorAxisLength;
            this.Orientation = blob.Orientation;
            this.LastSighting = blob.LastSighting;
            this.DateCreated = blob.DateCreated;
            this.ProjectedBoundingBox = blob.ProjectedBoundingBox;
            this.State = blob.State;
            this.Depth = blob.Depth;
        }

        public Blob(ConnectedComponent blob)
        {
            this.CameraID = -1;
            this.ID = -1;

            this.Velocity = Point2f.Zero;
            this.Area = blob.Area;
            this.Centroid = blob.Centroid;
            this.Contour = blob.Contour;
            this.MajorAxisLength = blob.MajorAxisLength;
            this.MinorAxisLength = blob.MinorAxisLength;
            this.Orientation = blob.Orientation;
            this.LastSighting = DateCreated = DateTime.Now;
            this.State = BlobState.Entered;
            this.ProjectedBoundingBox = new Rect();

        }

        public void Update(Blob blob)
        {
            this.ID = blob.ID;
            this.CameraID = blob.CameraID;

            var newCenter = new Point(blob.Contour.Rect.X + blob.Contour.Rect.Width / 2, blob.Contour.Rect.Y + blob.Contour.Rect.Height / 2);
            var dx = blob.Centroid.X - this.Centroid.X;
            var dy = blob.Centroid.Y - this.Centroid.Y;
            this.Velocity = new Point2f(dx, dy);

            this.Area = blob.Area;
            this.Centroid = blob.Centroid;
            this.Contour = blob.Contour;
            this.MajorAxisLength = blob.MajorAxisLength;
            this.MinorAxisLength = blob.MinorAxisLength;
            this.Orientation = blob.Orientation;
            this.LastSighting = blob.LastSighting;
            this.ProjectedBoundingBox = blob.ProjectedBoundingBox;
            this.Depth = blob.Depth;
        }
    }

    public class BlobCollection : Collection<Blob>
    {
        public BlobCollection(Size imageSize)
        {
            ImageSize = imageSize;
            CameraID = -1;
        }

        public BlobCollection(IList<Blob> blobs, Size imageSize, int cameraID)
            : base(blobs)
        {
            this.ImageSize = imageSize;
            this.CameraID = cameraID;
        }

        public BlobCollection(BlobCollection other)
        {
            foreach (var blob in other)
                Add(new Blob(blob));
            this.ImageSize = other.ImageSize;
            this.CameraID = other.CameraID;
        }

        public Size ImageSize { get; private set; }
        public int CameraID { get; set; }
    }
}
