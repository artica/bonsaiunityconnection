﻿using Bonsai;
using OpenCV.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Reactive.Linq;

namespace Bonsai.Artica.TSPS
{
    [Description("Load a blob collection from a CSV file")]
    public class BlobCollectionReader : Source<BlobCollection>
    {
        [FileNameFilter("CSV Files|*.csv")]
        [Editor("Bonsai.Design.OpenFileNameEditor, Bonsai.Design", typeof(UITypeEditor))]
        [Description("The name of the input CSV file.")]
        public string FileName { get; set; }

        [Description("Reference image size.")]
        public Size ImageSize { get; set; }

        public BlobCollectionReader()
        {
            ImageSize = new Size(1024, 768);
        }

        public override IObservable<BlobCollection> Generate()
        {
            return Observable.Defer(() =>
            {
                return Observable.Return(ReadCsv());
            });
        }

        private BlobCollection ReadCsv()
        {
            var blobCollection = new BlobCollection(ImageSize);
            if (string.IsNullOrEmpty(FileName)) return blobCollection;
            try
            {
                var now = DateTime.Now;
                using (var reader = new CsvFileReader(FileName))
                {
                    reader.Delimiter = ';';
                    var row = new List<string>();
                    while (reader.ReadRow(row))
                    {
                        if (row.Count != 5)
                            continue;

                        blobCollection.Add(new Blob()
                        {
                            CameraID = int.Parse(row[0]),
                            ID = int.Parse(row[1]),
                            LastSighting = DateTime.Parse(row[2]),
                            Centroid = new Point2f(float.Parse(row[3]), float.Parse(row[4]))
                        });
                    }
                }
                return blobCollection;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not read CSV : " + ex.Message);
                return blobCollection;
            }
        }
    }
}
