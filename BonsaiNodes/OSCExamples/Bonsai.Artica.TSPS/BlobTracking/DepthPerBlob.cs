﻿using Bonsai;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.ComponentModel;
using OpenCV.Net;
using System.Text;
using Bonsai.Vision;

namespace Bonsai.Artica.TSPS
{
    [Description("Custom DepthPerBlob.")]
    public class DepthPerBlob : Transform<Tuple<IplImage, BlobCollection>, Tuple<IplImage, BlobCollection>>
    {
        public override IObservable<Tuple<IplImage, BlobCollection>> Process(IObservable<Tuple<IplImage, BlobCollection>> source)
        {
            return source.Select(input =>
            {
                var sourceImage = input.Item1.Clone();

                var blobs = input.Item2;
                var resultingImage = new IplImage(sourceImage.Size, sourceImage.Depth, sourceImage.Channels);
                resultingImage.SetZero();
                foreach (var blob in blobs)
                {
                    double blobColor = 0;
                    int count = 0;
                    var rect = OpenCV.Net.CV.BoundingRect(blob.Contour);
                    var rect2 = blob.Contour.Rect;
                    for (var x = rect.X; x < rect.X + rect.Width &&  x<sourceImage.Width; x++)
                        for (var y = rect.Y; y < rect.Y + rect.Height && y< sourceImage.Height; y++)
                        {

                            var val = sourceImage[y, x].Val0;
                           //var val = sourceImage[]
                            if (OpenCV.Net.CV.PointPolygonTest(blob.Contour, new Point2f(x,y),false) >0 && val!=0)
                            {
                                blobColor += val;
                                count++;
                            }
                        }
                    if (count != 0)
                        blobColor /= count;
                    
                    OpenCV.Net.CV.Rectangle(resultingImage, rect, new Scalar(blobColor), -1);

                    //TODO: implement artimetics to obtain distances

                    blob.Depth = (float)blobColor;
                   // resultingImage.RegionOfInterest = rect;

                }
                return new Tuple<IplImage, BlobCollection>(resultingImage, input.Item2);
            });
        }
        /*
        public override IObservable<ConnectedComponentCollection> Process(IObservable<Contours> source)
        {
            return source.Select(input =>
            {
                var currentContour = input.FirstContour;
                var output = new ConnectedComponentCollection(input.ImageSize);

                while (currentContour != null)
                {
                    var component = ConnectedComponent.FromContour(currentContour);
                    currentContour = currentContour.HNext;
                    output.Add(component);
                }

                return output;
            });
        }

        public IObservable<ConnectedComponentCollection> Process(IObservable<Tuple<Contours, IplImage>> source)
        {
            return source.Select(input =>
            {
                var image = input.Item2;
                var contours = input.Item1;
                var currentContour = contours.FirstContour;
                var output = new ConnectedComponentCollection(image.Size);

                while (currentContour != null)
                {
                    var component = ConnectedComponent.FromContour(currentContour);
                    component.Patch = image.GetSubRect(component.Contour.Rect);
                    currentContour = currentContour.HNext;
                    output.Add(component);
                }

                return output;
            });
        }*/
    }


}


