﻿using Bonsai;
using OpenCV.Net;
using System;
using System.ComponentModel;
using System.Reactive.Linq;

namespace Bonsai.Artica.TSPS
{
    [Description("Draws the bounding box of the input contours.")]
    public class DrawBoundingBox : Transform<Tuple<IplImage, BlobCollection>, IplImage>
    {
        public override IObservable<IplImage> Process(IObservable<Tuple<IplImage, BlobCollection>> source)
        {
            return source.Select(input =>
            {
                var output = input.Item1.Clone();

                foreach (var blob in input.Item2)
                    CV.Rectangle(output, blob.Contour.Rect, Scalar.All(255), 2);
                return output;
            });
        }
    }
}
