# README #

This repository have the objective to give a help in the connection of the software Bonsai with Unity.
Bonsai is a software  you tell your computer what to do not through long listings of text but by manipulating graphical elements in a workflow.
We can use either Computer Vision, hardware like Arduino's and many others being very flexible for our projects.
This software is Node Based and we can create nodes using Python or C# if we need some custom nodes.
Unity is one of the most known game Engine now days with many features to explore being one of our elections for Interactives.
### Requirements ###

* Unity 2017.1 + - https://unity3d.com/get-unity/download/archive
* Bonsai - http://bonsai-rx.org/

### How do I set up? ###

* Unity it's a straight foward instalation just double click , next , choose where you want to save it and which addons we want to use, we recommend the typical for this project.
* Bonsai need to be downloaded and installed but after that we open the Bonsai.exe, click in Tools - >  Manage Packages.
* In this option we choose the OSC package or other packages we need to use ( We recommend Starter Package will install all basic's we want).

### Contribution guidelines ###

* ArticaCC
* Gon�alo Lopes - Bonsai Creator
